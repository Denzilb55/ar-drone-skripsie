﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARDrone.Control
{
    public class DataPoint
    {
        private double value;
        private long time;

        public double Value
        {
            get { return value; }
        }

        public long Time
        {
            get { return time; }
        }

        public DataPoint (double value, long time)
        {
            this.time = time;
            this.value = value;
        }
    }
    
    public class ForwardVelocityControl
    {
        public static ForwardVelocityControl Controller = new ForwardVelocityControl(); //Controller Singleton
        
        public LinkedList<DataPoint> inputPIDHistory = new LinkedList<DataPoint>();
        public const int HISTORY_LENGTH = 7;
        public const float SLEW_RATE_MAX = 8; // (m/s) / s
        public const float REFERENCE_VELOCITY_MAX = 4; 
        public const float PLANT_INPUT_SATURATION = 25;

        public DataPoint secondPoint;
        public double lastReference;

        private bool enabled = false;

        private double integratorValue = 0;

        
        private float pGain     = 1.056f;
        private float iGain     = 0.96f;
        private float dGain     = 0.84f;

        private double controllerOutput;

        public double ControllerOutput
        {
            get { return controllerOutput; }
        }

        private ForwardVelocityControl() { }

        public bool IsEnabled
        {
            get { return enabled; }
        }

        public void Enable()
        {
            enabled = true;
        }

        public void Disable()
        {
            enabled = false;
        }

        public void PushPoint(double measuredVelocity, long time, float referenceVelocity) 
        {
            if (!enabled) return;
            long deltaTime = 0;

            if (secondPoint != null)
            {
                deltaTime = time - secondPoint.Time; //ms
            }

            double limitedReference = GetLimitedReference(lastReference, referenceVelocity, deltaTime);

            RunControllerStep(deltaTime, time, limitedReference, measuredVelocity);
            lastReference = limitedReference;
        }

        //limit max reference and slew rate
        public static double GetLimitedReference(double lastReference, double currentReference, long deltaTime)
        {
            if (deltaTime == 0) return 0;
            
            double val = currentReference;

            double rate = 1000 * (currentReference - lastReference) / deltaTime;

            if (rate > SLEW_RATE_MAX)
            {
                val = lastReference + SLEW_RATE_MAX * deltaTime / 1000;
            }
            else if (rate < -SLEW_RATE_MAX)
            {
                val = lastReference - SLEW_RATE_MAX * deltaTime / 1000;
            }

            if (val > REFERENCE_VELOCITY_MAX)
            {
                val = REFERENCE_VELOCITY_MAX;
            }
            else if (val < -REFERENCE_VELOCITY_MAX)
            {
                val = -REFERENCE_VELOCITY_MAX;
            }

            return val;
        }

        private void RunControllerStep(long deltaTime, long time, double reference, double feedback)
        {
            double inputPID = reference - feedback;
            Console.WriteLine("RUNNINGSTEP");
            DataPoint dp = new DataPoint(inputPID, time);

            if (inputPIDHistory.First != null)
            {
                secondPoint = inputPIDHistory.First.Value;
            }
            inputPIDHistory.AddFirst(dp);

            if (inputPIDHistory.Count > HISTORY_LENGTH)
            {
                inputPIDHistory.RemoveLast();
            }

            //integrate
            if (secondPoint != null)
                integratorValue += (inputPID - 0.5 * (inputPID - secondPoint.Value)) * (time - secondPoint.Time) / 1000f; //modified euler integration

            //limit integrator windup
            if (integratorValue > PLANT_INPUT_SATURATION)
            {
                integratorValue = PLANT_INPUT_SATURATION;
            }
            else if (integratorValue < -PLANT_INPUT_SATURATION)
            {
                integratorValue = -PLANT_INPUT_SATURATION;
            }

            double outputPID = inputPID * pGain + GetApproximateDerivative(inputPIDHistory, 4) * dGain + integratorValue * iGain;

            if (outputPID > PLANT_INPUT_SATURATION)
            {
                outputPID = PLANT_INPUT_SATURATION;
            }
            else if (outputPID < -PLANT_INPUT_SATURATION)
            {
                outputPID = -PLANT_INPUT_SATURATION;
            }

            controllerOutput = outputPID;
        }

        //using least squares best fit
        public static double GetApproximateDerivative(LinkedList<DataPoint> historyList, int minPoints)
        {
            if (historyList.Count < minPoints) return 0;

            double xSum = 0;
            double ySum = 0;
            double xySum = 0;
            double xSquaredSum = 0;

            foreach (var p in historyList)
            {
                xSum += p.Time;
                ySum += p.Value;
                xySum += p.Time * p.Value;
                xSquaredSum += p.Time * p.Time;
            }

            int n = historyList.Count;

            double gradient = (n * xySum - xSum * ySum)/(n * xSquaredSum - xSum * xSum);

            return gradient * 1000;
        }
    }
}
